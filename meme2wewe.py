# MIT License
#
# Copyright (c) 2023 Joonas T. Holmi (jtholmi@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Required modules
import numpy as np
import PySimpleGUI as psg

# Built-in modules
from datetime import datetime
import json
import os
import random
from statistics import NormalDist
import textwrap

# Generate load and save locations
dateTimeNow = datetime.now()
dateTimeNowStr = dateTimeNow.strftime("%y%m%d %H%M%S")
jsonTestQuestionsFrom = "6g.json"
jsonTestAnswersTo = (
    f"{os.path.splitext(jsonTestQuestionsFrom)[0]} Anonymous {dateTimeNowStr}.json"
)

# GUI settings
psg.set_options(font=("Courier New", 12))
textWrapperWidth = 80
textWrapper = textwrap.TextWrapper(
    width=textWrapperWidth, break_long_words=True, replace_whitespace=False
)

# Initial GUI
layout = [
    [
        psg.Text("Load test questions from", size=(25, 1)),
        psg.Input(default_text=jsonTestQuestionsFrom),
        psg.FileBrowse(file_types=(("JSON files", "*.json"),)),
    ],
    [
        psg.Text("Save test answers to", size=(25, 1)),
        psg.Input(default_text=jsonTestAnswersTo),
        psg.FileBrowse(file_types=(("JSON files", "*.json"),)),
    ],
    [
        psg.Button("Begin"),
        psg.Column(
            [[psg.Button("Exit")]],
            pad=(0, 0),
            element_justification="right",
            expand_x=True,
        ),
    ],
]

window = psg.Window(f"Prepare test", layout)
event, values = window.read(close=True)
if event != "Begin":
    exit(0)
if values[0] != "" and os.path.isfile(values[0]):
    jsonTestQuestionsFrom = values[0]
if values[1] != "":
    jsonTestAnswersTo = values[1]

# Load test (with possible default answers)
test = {}
with open(jsonTestQuestionsFrom, "r", encoding="utf8") as file:
    test = json.load(file)

testNote = test.get("NOTE", "")
testKeys = list(test["KEY"].keys())
testValues = list(test["VALUE"].keys())
testValuesMaxLen = max([len(v) for v in testValues])

testValueCounts = test["VALUE"]

testRatingAnswers = [int(k) for k in test["RATING"]["Answer"].keys()]
testRatingAnswersMin = min(testRatingAnswers)
testRatingAnswersMax = max(testRatingAnswers)
testRatingAnswersMean = (testRatingAnswersMin + testRatingAnswersMax) / 2

testQuestions = test["QUESTION"]

questionList = []
keyDict = {}

# If note exists, then show it
if testNote != "":
    psg.popup_ok(testNote, title="Note")


# Add test questions and corresponding key weights
for category in testQuestions:
    for subcategory in testQuestions[category]:
        for question in testQuestions[category][subcategory]:
            info = testQuestions[category][subcategory][question]
            keyWeights = {
                k: v / len(testQuestions[category])
                for k, v in info.items()
                if k in testKeys
            }
            questionList.append((question, keyWeights, (category, subcategory)))
            for key, weight in keyWeights.items():
                keyDict.setdefault(key, {"ratings": [], "weights": []})


# Run test
answeredQuestions = 0

groups = {}
for question, keyWeights, groupIndex in questionList:
    groups.setdefault(groupIndex, []).append((question, keyWeights))

# Shuffle test group keys but try avoid subsequent test groups side-by-side
groupKeys = list(groups.keys())
groupIndices = []
groupIndicesPoolForbidden = []
groupIndicesPool = list(range(len(groupKeys)))
for i in range(len(groupKeys) - 1, -1, -1):
    if len(groupIndicesPool) == 0:
        groupIndicesPool = groupIndicesPoolForbidden
        groupIndicesPoolForbidden = []
    index = groupIndicesPool.pop(random.randint(0, i - len(groupIndicesPoolForbidden)))
    groupIndices.append(index)
    groupIndicesPool += groupIndicesPoolForbidden
    groupIndicesPoolForbidden = []
    if index - 1 in groupIndicesPool:
        groupIndicesPoolForbidden.append(
            groupIndicesPool.pop(groupIndicesPool.index(index - 1))
        )
    if index + 1 in groupIndicesPool:
        groupIndicesPoolForbidden.append(
            groupIndicesPool.pop(groupIndicesPool.index(index + 1))
        )
groupKeys = [groupKeys[i] for i in groupIndices]

# Ask groups of questions
i = 0
for groupIndex in groupKeys:
    groupQuestions = groups[groupIndex]
    random.shuffle(groupQuestions)
    # Create the window
    layout = []
    layout.append(
        [
            psg.Text(
                textWrapper.fill(groupIndex[0])
                + "\n\n"
                + textWrapper.fill(test["RATING"]["Question"])
                + "\n\n"
                + textWrapper.fill(groupIndex[1])
            )
        ]
    )
    j = 0
    for question, keyWeights in groupQuestions:
        question_i = textWrapper.fill(question)
        rating_i = testQuestions[groupIndex[0]][groupIndex[1]][question].get(
            "RATING", testRatingAnswersMean
        )
        layout.append(
            [
                psg.Frame(
                    f"({i+1+j}.)",
                    [
                        psg.vtop(
                            [
                                psg.Slider(
                                    range=(
                                        testRatingAnswersMin,
                                        testRatingAnswersMax,
                                    ),
                                    default_value=rating_i,
                                    tick_interval=1,
                                    resolution=1,
                                    disable_number_display=False,
                                    orientation="horizontal",
                                    key=j,
                                    tooltip=", ".join(
                                        [
                                            f"{k} = {v}"
                                            for k, v in test["RATING"]["Answer"].items()
                                        ]
                                    ),
                                ),
                                psg.Text(question_i),
                            ]
                        )
                    ],
                    expand_x=True,
                )
            ]
        )
        j += 1
    layout.append(
        [
            psg.Button("Next"),
            psg.Column(
                [[psg.Button("Exit")]],
                pad=(0, 0),
                element_justification="right",
                expand_x=True,
            ),
        ]
    )
    window = psg.Window(f"Questions {i+1}-{i+j}/{len(questionList)}:", layout)
    answers = None
    event, values = window.read(close=True)
    if event == "Next":
        answers = [int(v) for v in values.values()]
    if answers is None:
        exit(0)
    j = 0
    for question, keyWeights in groupQuestions:
        i += 1
        question_i = (
            textWrapper.fill(groupIndex[0])
            + "\n\n"
            + textWrapper.fill(test["RATING"]["Question"])
            + "\n\n"
            + textWrapper.fill(groupIndex[1].replace("...", question))
            + "\n"
        )
        testQuestions[groupIndex[0]][groupIndex[1]][question]["RATING"] = answers[j]
        testQuestions[groupIndex[0]][groupIndex[1]][question]["#"] = i

        for key, weight in keyWeights.items():
            keyDict[key]["ratings"].append(answers[j])
            keyDict[key]["weights"].append(weight)
        answeredQuestions += 1
        j += 1

# On test completion, compute and show results for each key
confidenceIntervalInPercentage = test.setdefault("RESULT", {}).setdefault(
    "Confidence Interval, %", 80
)
finalResult = {}
for key, data in keyDict.items():
    ratings = np.array(data["ratings"])
    weights = np.array(data["weights"])
    ratingsNormalized = (
        2
        * (ratings - testRatingAnswersMin)
        / (testRatingAnswersMax - testRatingAnswersMin)
        - 1
    )  # Normalize to [-1, 1]
    # Reverse code: Convert negative weights to positive weights by changing signs of normalized ratings
    ratingsNormalized = np.sign(weights) * ratingsNormalized
    weights = np.abs(weights)

    weightsNormalized = weights / np.sum(weights)  # Normalize sum of weights to 1

    scores = ratingsNormalized * weightsNormalized
    # Weighted mean
    meanScore = np.sum(scores)
    # Effective number of measurements (to correct for loss of degrees of freedom)
    nEff = 1 / np.sum(weightsNormalized**2)
    # Descriptive statistic: Weighted standard deviation (unbiased)
    stdScore = np.sqrt(
        np.sum(weightsNormalized * (scores - meanScore) ** 2)
        / np.sum(weightsNormalized)
        / (1 - 1 / nEff)  # Unbiased correction factor
    )
    # Inferential statistic: Weighted standard error or uncertainty (unbiased)
    seScore = stdScore / np.sqrt(nEff)  # 68.268949% confidence interval
    # Probability for each value
    valueProbabilities = {}
    pNotPrev = 1
    for i, value in enumerate(testValues):
        if i < len(testValues) - 1 and seScore != 0:
            z = (
                (1 - 2 * (i + 1) / len(testValues)) - meanScore
            ) / seScore  # Assume equal-distance between possible values in test
            p = 1 - NormalDist().cdf(z)  # Pr(Z>z)
        else:
            p = 1  # Pr(Z>-inf)
        valueProbabilities[value] = (
            p * pNotPrev
        )  # Pr(Z>z and Z<=zPrev) = Pr(Z>z) * Pr(Z<=zPrev)
        pNotPrev = 1 - p
    # Confidence interval
    ciScore = (
        NormalDist().inv_cdf(1 - (100 - confidenceIntervalInPercentage) / 200) * seScore
    )  # Convert to X% confidence interval
    # Final results
    finalResult[key] = (meanScore, seScore, ciScore, valueProbabilities)
    test.setdefault("RESULT", {}).setdefault(key, {})["Mean"] = meanScore
    test.setdefault("RESULT", {}).setdefault(key, {})["Standard Error"] = seScore
    test.setdefault("RESULT", {}).setdefault(key, {})["Confidence Interval"] = ciScore
    for value in testValues:
        test.setdefault("RESULT", {}).setdefault(key, {}).setdefault(
            "Probabilities", {}
        )[value] = valueProbabilities[value]
finalResultSorted = sorted([(v, k) for k, v in finalResult.items()])[::-1]


# Prepare for the final result window
layout = []
text = []
text.append(
    f"<TYPE>: <ENERGY \u00B1 {confidenceIntervalInPercentage:.0f}%-CONF.-INT.> % (<RESULT>) [Prob.: <P.> % <RESULT>, ...]"
)
for i, finalResult in enumerate(finalResultSorted):
    finalResult, key = finalResult  # Extract key
    (
        meanScore,
        seScore,
        ciScore,
        valueProbabilities,
    ) = finalResult  # Extract all else
    # Assign key value based on counts
    keyValue = ""
    cumulativeCount = 0
    for k, v in testValueCounts.items():
        keyValue = k
        cumulativeCount += v
        if i < cumulativeCount:
            break
    test["RESULT"][key]["Value"] = keyValue  # Store key value to JSON
    keyValue = f"({keyValue})"  # Add ()-brackets
    test_i_probabilities = ", ".join(
        [
            f"{100*ps[0]:.1f} % {ps[1]}"
            for j, ps in enumerate(
                sorted(
                    [(v, k) for k, v in valueProbabilities.items()],
                    reverse=True,
                )
            )
            if j < len(testValues) - 1
        ]
    )
    text_i = f"{key}: {100*meanScore:+5.1f} \u00B1 {100*ciScore:4.1f} % {keyValue:{testValuesMaxLen+2}s} [Prob.: {test_i_probabilities}]"
    text.append(text_i)

# Save answers to file
with open(jsonTestAnswersTo, "w", encoding="utf8") as file:
    json.dump(test, file, indent=4)

textLines = len(text)
text = "\n".join(text)
layout.append(
    [
        psg.Multiline(
            text, size=(textWrapperWidth, textLines), disabled=True, no_scrollbar=True
        )
    ]
)
layout.append(
    [
        [
            psg.Column(
                [[psg.Button("Exit")]],
                pad=(0, 0),
                element_justification="right",
                expand_x=True,
            ),
        ]
    ]
)
window = psg.Window(f"Final result:", layout)
_, _ = window.read(close=True)
